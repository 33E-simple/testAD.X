/*! \file  serialInitialize.c
 *
 *  \brief Initialize the UART
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 9:17 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

//! Instruction Cycle Frequency
#define FCY             70000000
//! Desired baud rate
#define BAUDRATE         38400
//! Value for the baud rate generator
#define BRGVAL          ((FCY/BAUDRATE)/16)-1


/*! Initialize the UART */
/*! serialInitialize() initializes UART1 to the selected baud rate and the
 *  desired set of pins.
 *
 * \param bAlternate int 0 to use default UART pins, 1 to use alternate pins
 * \returns none
 */
void serialInitialize( void )
{
    _TRISB8 = 0;            // Make RB8 an output
    RPOR3bits.RP40R = 1;    // Set U1TX to RP40 (pin 17, RB8)

      // UART setup
    U1BRG  = BRGVAL;        // Set up baud rate generator
    
    U1MODEbits.UARTEN = 1;  // UART1 enables
    U1MODEbits.USIDL = 0;   // Continue in idle mode
    U1MODEbits.IREN = 0;    // IrDE disabled
    U1MODEbits.UEN = 0;     // RTS, CTS, BBCLK1 unused
    U1MODEbits.WAKE = 0;    // Wake on start bit disabled
    U1MODEbits.LPBACK = 0;  // Loopback mode disabled
    U1MODEbits.ABAUD = 0;   // Autobaud disabled
    U1MODEbits.BRGH = 0;    // High baud rate disabled
    U1MODEbits.PDSEL = 0;   // 8 bit, no parity
    U1MODEbits.STSEL = 1;   // 2 stopbits for now

    U1STAbits.UTXISEL0 = 0; //UTXISEL interrupt when empty, these two
    U1STAbits.UTXISEL1 = 1; // bits are curiously separate
    U1STAbits.UTXINV = 0;   // Idle state 1
    U1STAbits.UTXEN = 1;    // Enable transmit

    _U1RXIF=0;              // Clear UART RX interrupt Flag
    _U1TXIF = 0;            // Clear the UART TX interrupt flag
}

