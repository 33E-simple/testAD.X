/*! \file  TFTsetColorX.c
 *
 *  \brief Set the TFT drawing color
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 1:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/*! TFTsetColorX - Set the drawing color */

/*!
 *
 */
void TFTsetColorX( unsigned int uColor )
{
  putchSerial(0x04);
  sendWord(uColor);
}
