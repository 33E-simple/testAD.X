/*! \file  testAD.h
 *
 *  \brief Constants and prototypes for testAD
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 9:20 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TESTAD_H
#define	TESTAD_H

#ifdef	__cplusplus
extern "C"
{
#endif

/* Global initialization */
void Initialize(void);

/*! Read a value from the A/D */
unsigned int ADCread( int );

/*! Send a character over the serial port */
void putchSerial( unsigned char );

/*! Initialize the UART */
void serialInitialize( void );

/*! ADCinitialize - Initialize the A/D converter */
void ADCinitialize(void);

/*! T1initialize - Initialize TMR1 */
void T1initialize(void);

/*! Send a 16 bit word to the terminal */
void sendWord( unsigned );

/*! Send a bunch of nulls to guarantee sync with terminal */
void idleSync(void);

/*! Wait a short time */
void waitAshort( int );

/*! Send initialization string to the TFT */
void TFTinitialize( void );

/*! TFTline - Draw a line on the TFT */
void TFTline(unsigned int, unsigned int, unsigned int, unsigned int);

/*! TFTclear - Clear the TFT display */
void TFTclear(void );

/*! TFTsetColorX - Set the drawing color */
void TFTsetColorX( unsigned int );

/*! TFTsetBackColorX - Set the screen background color */
void TFTsetBackColorX( unsigned int );

/*! TFTclearRect - Clear a rectangle with an RGB color */
void TFTclearRect( unsigned char, unsigned char, unsigned char,
        int, int, int, int );

/*! TFTinteger - Display a 3 character integer */
void TFTinteger(int, int, int );

#define ADCHANNEL       5

#define WHITE           0xffff
#define SPRINGGREEN     0x07ef
#define BLACK           0x0000
#define DARKBLUE        0x0011
#define DIMGRAY         0x6b4d
#define DARKGRAY        0xad55
#define INDIANRED       0xcaeb
#define DARKRED         0x8800

#ifdef	__cplusplus
}
#endif

#endif	/* TESTAD_H */

