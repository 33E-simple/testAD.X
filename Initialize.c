/*! \file  Initialize.c
 *
 *  \brief Initilize everything
 *
 *
 *  \author jjmcd
 *  \date June 9, 2015, 2:52 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/LCD.h"
#include "testAD.h"

#define DEFAULT_SPEED 74 // 70MIPS

/*! Initialize - Inititlize the testAD application and peripherals */

/*!
 *
 */
void Initialize(void)
{

  // Wait a while to prevent programmer from doing weird stuff
  Delay_ms(2000L*23L/70L);  // Should be close to 2 second

  // Default clock
  CLKDIVbits.FRCDIV = 0;    // Divide by 1 = 8MHz
  CLKDIVbits.PLLPRE = 0;    // Divide by 2 = 4 MHz
  PLLFBD = DEFAULT_SPEED;   // Multiply by 58 = 232
  CLKDIVbits.PLLPOST = 0;   // Divide by 2 = 116

  _TRISB6 = 0;

  T1initialize();
  ADCinitialize();
  LCDinit();
  serialInitialize();

  LCDclear();
  LCDputs("Waiting");
  Delay_ms(3000);
  LCDclear();

  TFTinitialize();

}
