/*! \file  sendWord.c
 *
 *  \brief Send a 16-bit word out the serial port
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 10:19 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"


/*! Send a 16 bit word to the terminal */
void sendWord( unsigned int word )
{
  putchSerial(word&0xff);
  putchSerial((word&0xff00)>>8);
}
