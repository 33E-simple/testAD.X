/*! \file  TFTline.c
 *
 *  \brief Draw a line on the TFT
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 10:33 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"


/*! TFTline - Draw a line on the TFT */

/*!
 *
 */
void TFTline(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
  putchSerial( 0x0b );
  putchSerial( x1&0xff );
  putchSerial( (x1>>8)&0xff );
  putchSerial( y1&0xff );
  putchSerial( (y1>>8)&0xff );
  putchSerial( x2&0xff );
  putchSerial( (x2>>8)&0xff );
  putchSerial( y2&0xff );
  putchSerial( (y2>>8)&0xff );

}
