/*! \file  ADCinitialize.c
 *
 *  \brief Initialize the A/D converter
 *
 *
 *  \author jjmcd
 *  \date June 9, 2015, 4:42 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! ADCinitialize - Initialize the A/D converter */

/*!
 *
 */
/*! Initialize the analog to digital converter */
void ADCinitialize( void )
{
  // ADC Control registers
  AD1CON1bits.ADSIDL = 1;   // Discontinues module operation when device enters Idle mode
  AD1CON1bits.FORM = 0;     // Absolute decimal result, unsigned, right justified
//  AD1CON1bits.MODE12 = 0;   // 10-bit A/D operation
//  AD1CON1bits.SSRC = 0;     // Clearing the Sample bit ends sampling and starts conversion
  AD1CON1bits.SSRC = 7;     // Internal counter ends sampling and starts convert
  AD1CON2 = 0;              // All defaults
  AD1CON3 = 0;              // All defaults
  AD1CON3bits.SAMC = 16;    // Auto sample 15Tad
  AD1CON3bits.ADCS = 63;    // Conversion clock 64 Tcy
//  AD1CON5 = 0;              // All defaults

  // A/D Sample Select Register
//  AD1CHS = 0;               // Channel 0
    AD1CHS0bits.CH0SA = 24;

  // A/D input scan select register low word
  AD1CSSL = 0;              // No scanning

  AD1CON1bits.ADON = 1;     // Turn on ADC

  AD1CON1bits.SAMP = 1;     // Start sample
//  _AD1IE = 1;               // Enable interrupt
}
