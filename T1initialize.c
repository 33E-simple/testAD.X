/*! \file  T1initialize.c
 *
 *  \brief Initialize timer 1
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 9:31 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! T1initialize - Initialize TMR1 */

/*!
 *
 */
void T1initialize(void)
{
  T1CONbits.TSIDL = 0;  // Continues module operation in Idle mode
  T1CONbits.TGATE = 0;  // Gated time accumulation is disabled
  T1CONbits.TCKPS = 0;  // Timer1 Input Clock Prescale 1:1
  T1CONbits.TSYNC = 0;  // External clock input is not synchronized
  T1CONbits.TCS   = 0;  // Clock source Internal clock
  T1CONbits.TON   = 1;  // Timer 1 on

  PR1 = 21554;          // Timer 1 compare register
  PR1 /= 16;

  TMR1 = 0;             // Clear timer 1 register
  _T1IE = 0;            // Disable timer 1 interrupt
  _T1IF = 0;            // Clear timer 1 interrupt flag
}
