/*! \file  testAD.c
 *
 *  \brief Display audio samples on the TFT
 *
 *
 *  \author jjmcd
 *  \date June 9, 2015, 4:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include "configBits.h"
#include "../include/LCD.h"
#include "testAD.h"

// Array of measurements
unsigned int nMeas[320];

/*! main - */

/*!
 *
 */
int main(void)
{
  int i; // Misc. counting
  char szWork[32]; // String for display
  int nYvalue; // Current Y value on graph
  int nYlast; // Previous Y value on graph
  int nYmaxValue; // Maximum Y value

  /* Initialize everything */
  Initialize();

  /* Clear the screen to gray */
  TFTsetBackColorX(DARKGRAY);
  TFTclear();

  /* Draw axes */
  TFTsetColorX(WHITE);
  TFTline(24, 10, 24, 220);
  TFTline(24, 221, 319, 221);

  // Set the font to small
  putchSerial(0x0c);
  putchSerial(1);

  while (1)
    {
      // Do a measurement each time the timer expires
      for (i = 0; i < 320; i++)
        {
          // Wait for the timer to expire
          while (!_T1IF)
            ;
          // Clear the timer interrupt flag
          _T1IF = 0;
          // Turn on the LED to notify sampling
          _LATB6 = 1;
          // Get a value from the A/D
          nMeas[i] = ADCread(ADCHANNEL);
          // Turn off the LED
          _LATB6 = 0;
        }
      // Display the first 8 measurements on the LCD
      LCDclear();
      sprintf(szWork, "%03x %03x %03x %03x",
              nMeas[0], nMeas[1], nMeas[2], nMeas[3]);
      LCDputs(szWork);
      sprintf(szWork, "%03x %03x %03x %03x",
              nMeas[4], nMeas[5], nMeas[6], nMeas[7]);
      LCDline2();
      LCDputs(szWork);

      // Draw a graph of the first 240 measurements
      idleSync();

      // Clear drawing area very dark blue
      TFTclearRect(0, 0, 8, 25, 10, 319, 220);

      // Calculate the maximum value to show on the graph
      nYmaxValue = 0;
      for (i = 0; i < 290; i++)
        if (nMeas[i] > nYmaxValue)
          nYmaxValue = nMeas[i];
      nYmaxValue += 30;
      nYlast = 239 - 240 * nMeas[0] / nYmaxValue;
      if (nYlast > 239)
        nYlast = 239;
      if (nYlast < 0)
        nYlast = 0;

      // Show the Y axis values
      TFTsetColorX(DARKRED);
      TFTinteger(1, 5, nYmaxValue);
      TFTinteger(1, 108, nYmaxValue / 2);
      TFTinteger(1, 215, 0);

      // Set the color to SPRINGGREEN
      TFTsetColorX(SPRINGGREEN);
      // Now display the actual graph
      for (i = 1; i < 280; i++)
        {
          nYvalue = 239 - 240 * nMeas[i] / nYmaxValue;
          if (nYvalue > 239)
            nYvalue = 239;
          if (nYvalue < 0)
            nYvalue = 0;
          TFTline(i - 1 + 25, nYlast - 20, i + 25, nYvalue - 20);
          nYlast = nYvalue;
        }
      Delay_ms(2000);
    }

  return 0;
}
