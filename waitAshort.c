/*! \file  waitAshort.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 10:20 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! Wait a short time */
void waitAshort( int len )
{
  int i,j;

  for ( j=0; j<len; j++ )
    for (i=0; i<1000; i++ )
        ;
}
