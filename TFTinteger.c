/*! \file  TFTinteger.c
 *
 *  \brief Display a 3 digit integer on the TFT
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 8:22 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "testAD.h"

/*! TFTinteger - Display a 3 character integer */

/*!
 *
 */
void TFTinteger(int x, int y, int num )
{
  char szWork[16];
  int i;
  
  sprintf(szWork,"%3d",num);
  for ( i=0; i<3; i++ )
    {
      putchSerial(0x0d);
      putchSerial(szWork[i]);
      sendWord(x+7*i);
      sendWord(y);
    }
}
