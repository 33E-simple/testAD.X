/*! \file  initializeTFT.c
 *
 *  \brief Initialize the TFT
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 10:24 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/* TFT initialization string */
static unsigned char szSetup[] = {
  0x01,0x01,            // Initialize landscape
  0x05,0x00,0x00,       // SetBackColorX Black
  0x04,0xef,0x07,       // SetColorX SpringGreen
  0x06,                 // Clear
  0x00, 0x00, 0x00      // Idle
};

/*! Send initialization string to the TFT */
void TFTinitialize( void )
{
  int i;

  for ( i=0; i<12; i++ )
    putchSerial(szSetup[i]);
}
