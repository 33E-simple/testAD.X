/*! \file  TFTclear.c
 *
 *  \brief Clear the TFT screen
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 1:18 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/*! TFTclear - Clear the TFT display */

/*!
 *
 */
void TFTclear(void)
{
    putchSerial(0x06);
}
