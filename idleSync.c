/*! \file  idleSync.c
 *
 *  \brief Send a series of nulls to the TFT
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 10:21 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/*! Send a bunch of nulls to guarantee sync with terminal */
void idleSync(void)
{
  int i;
  for ( i=0; i<100; i++ )
    {
      putchSerial(0);
      waitAshort(10);
    }
}
