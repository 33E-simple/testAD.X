/*! \file  ADCread.c
 *
 *  \brief Read a value from the A/D converter
 *
 *
 *  \author jjmcd
 *  \date June 9, 2015, 4:49 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>


/*! ADCread - Read a value from the A/D converter */

/*!
 *
 */
unsigned int ADCread( int nChannel )
{
  int i;

  AD1CON1bits.ADON = 0;       // Turn off the A/D
  AD1CHS0bits.CH0SA = nChannel;       // Select desired channel

  AD1CON1bits.ADON = 1;       // Turn on the A/D

  AD1CON1bits.DONE = 0;       // Let A/D know it's notdone
  AD1CON1bits.SAMP = 1;       // Start sample

  for ( i=0; i<100; i++ )     // Give time for sampling
    ;
  AD1CON1bits.SAMP = 0;       // Stop sample

  while  ( !AD1CON1bits.DONE )// Wait for conversion
    ;

  while ( !_AD1IF )
    ;
  _AD1IF = 0;

  AD1CON1bits.SAMP = 1;       // Start sample
  return ADC1BUF0;

}
