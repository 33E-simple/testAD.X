/*! \file  TFTclearRect.c
 *
 *  \brief Clear a rectangle to an RGB color
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 1:33 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/*! TFTclearRect - Clear a rectangle with an RGB color */

/*!
 *
 */
void TFTclearRect( unsigned char r, unsigned char g, unsigned char b,
        int x1, int y1, int x2, int y2)
{
            putchSerial(0x09);
            putchSerial(r);
            putchSerial(g);
            putchSerial(b);
            sendWord(x1);
            sendWord(y1);
            sendWord(x2);
            sendWord(y2);

}
