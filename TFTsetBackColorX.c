/*! \file  TFTsetBackColorX.c
 *
 *  \brief Set the TFT background color
 *
 *
 *  \author jjmcd
 *  \date June 10, 2015, 1:29 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "testAD.h"

/*! TFTsetBackColorX - Set the screen background color */

/*!
 *
 */
void TFTsetBackColorX( unsigned int uColor )
{
  putchSerial(0x05);
  sendWord(uColor);

}
